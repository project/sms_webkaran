Description
===========
This module allows the SMS Framework (http://drupal.org/project/smsframework)
to use WebKaran (http://sms.webkaran.ir) as a gateway to send SMS.

Dependencies
============
- SMS Framework module
- NuSOAP library (sites/*/libraries/nusoap/nusoap.php)
